#include "apue.h"
#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

	DIR *dp;
	struct dirent **fileList = NULL;
	struct dirent *dirp = NULL;
	char *curDir = NULL;
	int x = NULL;
	int y = NULL;
	int count = NULL;
	int indexOf = NULL;

int compareSize(const void *const A, const void *const B)
{
    return strcmp((*(struct dirent **) A)->d_name, (*(struct dirent **) B)->d_name);
}

int main(int argc, char *argv[]) {
	
	if(argc == 1)
	{
		//if there are no arguments print current directory with no '.' files
		dp = opendir(".");
		x = scandir(".", &fileList, 0, alphasort);//sets x to the number of entries in fileList		
		
		//loop and print until we reach the end of listed enteries in fileList
		for(y = 0; y < x; y++)
		{
			if(fileList[y]->d_name[0] != '.')
			{
				printf("%s \t", fileList[y]->d_name);
				free(fileList[y]);
			}
		}
		printf("\n");
	}
	else if(argc == 2)
	{
		//checks that the flags are passed in the correct order
		if(strcmp(argv[1], "-a") == 0)
		{
			printf("flags should be given AFTER any given file path. Program will now terminate :(");
			exit(0);
		}
		else
		{
			
			dp = opendir(argv[1]);//sets dp to be the directory passed in by the user
			x = scandir(argv[1], &fileList, 0, alphasort);//sets x to the number of entries in fileList		

			//loop and print until we reach the end of listed enteries in fileList
			for(y = 0; y < x; y++)
			{
				if(fileList[y]->d_name[0] != '.')
				{	
					printf("%s\t", fileList[y]->d_name);
					free(fileList[y]);
				}
			}
			printf("\n");//for better terminal output formating
		}	
	}else if(argc == 3)
	{
		dp = opendir(argv[1]);//sets dp to the sirectory passed in by the user
		
		while( (dirp = readdir(dp)) != NULL)
			count++;//Number of Entries in our directory

		fileList = malloc(count *sizeof(*fileList));//allocates memory for later use		
		rewinddir(dp);//rewinds the directory back to starting positon
		count = 0;

		//copies dirp's content into fileList to be sorted with qsort
		while( (dirp = readdir(dp)) != NULL)
			fileList[count++] = dirp;

		//sorts the fileList into alphabetical order WITH the '.' files :)
		qsort(fileList, count, sizeof(*fileList), compareSize);
		
		for(indexOf = 0; indexOf < count ; indexOf++)
			printf("%s \t", fileList[indexOf]->d_name);
	
		printf("\n");//for better terminal output formating

	}
	closedir(dp);
	exit(0);
}
