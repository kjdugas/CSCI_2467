#!/bin/bash

if [[ $1 =~ [0-9] ]]; then
    echo "$1 matches a number between 0-9 somewhere in the string"
fi
