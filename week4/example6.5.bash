#!/bin/bash

if [[ $1 =~ ^[0-9]{3}$ ]]; then
    echo "$1 matches exactly a three digit number"
fi
