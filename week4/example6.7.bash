#!/bin/bash

if [[ $1 =~ ^[0-9]{3,7}$ ]]; then
    echo "$1 matches exactly a 3,4,5,6,or 7 digit number"
fi
