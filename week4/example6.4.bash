#!/bin/bash

if [[ $1 =~ [0-9]{3} ]]; then
    echo "$1 matches three number between 0-9 concurrently somewhere in the string"
fi
