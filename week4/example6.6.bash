#!/bin/bash

if [[ $1 =~ ^[0-9]{3,4}$ ]]; then
    echo "$1 matches exactly a three or four digit number"
fi
