#!/bin/bash

PROMPT="This is my first shell script"
SUPERDUDE="cmsumma"
X=10

echo $PROMPT

if [ "$USER" = "$SUPERDUDE" ]; then
    echo WOW, I am so honored to be in your presence
fi 

if [ $X -gt 5  ]; then
    echo X is greater than 5
fi

if [ $X -eq 10  ]; then
    echo X is equal to 10
fi
