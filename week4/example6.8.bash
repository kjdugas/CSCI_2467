#!/bin/bash

if [[ $1 =~ ^[A-Z][a-z]{1,}$ ]]; then
    echo "$1 seems like a reasonable spelling for a first name"
fi
