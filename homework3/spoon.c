#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

//global variables to be given scope within setWaitTimes()
int childWaitTime;
int parentWaitTime;
int grandChildWaitTime;

int main (int ac, char *argv[])
{
	if(ac > 2)
	{
		printf("TOO MANY ARGUMENTS, EVERYBODY SCREMMMMMMMM");
		exit(-1);//exits the currently running process
	}

	int choice = atoi(argv[1]);// converts the char pointer representing a line argument, into an int for use in setWaitTimes()
	setWaitTimes(choice);//sets wait time based on user input(1,2, or 3)
	
	pid_t processID = fork();//first fork, child process begins from here
	
	//if fork() failed do this 
	if(processID < 0)
	{
		perror("fork failed, YOU DUN GOOFED");
	}
	
	if(processID == 0)
	{
		//Child does stuff here
                printf("BEFORE SLEEP:I am the CHILD\n My ID is: %d\n Parent ID: %d\n My Group ID: %d\n Session ID: %d\n", getpid(), getppid(), getgid(), getsid(getpid()));

		        //second fork done inside of this conditonal spawing the GrandChild to execute code within this if
			if(processID = fork() == 0)
			{
				//Grandchild does stuff here
				printf("\nBEFORE SLEEP:I am the GRANDCHILD\n My ID is: %d\n Parent ID: %d\n My Group ID: %d\n Session ID: %d\n", getpid(), getppid(), getgid(), getsid(getpid()));
				sleep(grandChildWaitTime);
				printf("\nAFTER SLEEP, I AM NOW TERMINATED:I am the GRANDCHILD\n My ID is: %d\n Parent ID: %d\n My Group ID: %d\n Session ID: %d\n", getpid(), getppid(), getgid(), getsid(getpid()));
				exit(0);
			}
		sleep(childWaitTime);
	        printf("\nAFTER SLEEP, I AM NOW TERMINATED:I am the CHILD\n My ID is: %d\n Parent ID: %d\n My Group ID: %d\n Session ID: %d\n", getpid(), getppid(), getgid(), getsid(getpid()));
		wait(0);//waits until all currently running child process's terminate, this wait() waits for the child's childern to terminate(so only wait for the Grandchild)
		exit(0);
	}else
	{
		//Parent does stuff here
 		sleep(1);// this sleep is only here to resolve an issue with the output text getting mixed up
                printf("\nBEFORE SLEEP:I am the PARENT\n My ID is: %d\n Parent ID: %d\n My Group ID: %d\n Session ID: %d\n", getpid(), getppid(), getgid(), getsid(getpid()));
		sleep(parentWaitTime);
		printf("\nAFTER SLEEP, I AM NOW TERMINATED:I am the PARENT\n My ID is: %d\n Parent ID: %d\n My Group ID: %d\n Session ID: %d\n", getpid(), getppid(), getgid(), getsid(getpid()));
	}
	wait(0);//wait(0) makes the calling process wait for all of its' children to terminate
	return(0);
}

void setWaitTimes(int choice)
{
	if(choice == 1)
	{
		//grandchild finises first, then child , then parent
		childWaitTime = 6;
		parentWaitTime = 9;
		grandChildWaitTime = 3;
	}
	
	if(choice == 2)
	{	
		//child finises first, then parent, then grandchild
		childWaitTime = 3;
		parentWaitTime = 6;
		grandChildWaitTime = 9;
	}
	
	if(choice == 3)
	{
		//parent finises first, then child, then grandchild
		childWaitTime = 6;
		parentWaitTime = 3;
		grandChildWaitTime = 9;
	}
}
