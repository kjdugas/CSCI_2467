This is a simple program that forks itself once to create a child, then ONLY the child will fork once more to create a grandchild.
	once all the forking is done each process(parent/child/gandchild) will sleep for a preset amount of time based on the user input

1 ------> Grandchild terminates, then Child, then Parent
2 ------> Child terminates, then Parent, then Grandchild
3 ------> Parent terminates, then Child, then Grandchild

To compile: gcc spoon.c -o spoon

===========================================================================================================
./SPOON 3 OUTPUT TEXT (parent dies first)
BEFORE SLEEP:I am the CHILD
 My ID is: 5078
 Parent ID: 5077
 My Group ID: 1820637044
 Session ID: 4256

BEFORE SLEEP:I am the GRANDCHILD
 My ID is: 5079
 Parent ID: 5078
 My Group ID: 1820637044
 Session ID: 4256

BEFORE SLEEP:I am the PARENT
 My ID is: 5077
 Parent ID: 4256
 My Group ID: 1820637044
 Session ID: 4256

AFTER SLEEP, I AM NOW TERMINATED:I am the PARENT
 My ID is: 5077
 Parent ID: 4256
 My Group ID: 1820637044
 Session ID: 4256

AFTER SLEEP, I AM NOW TERMINATED:I am the CHILD
 My ID is: 5078
 Parent ID: 5077
 My Group ID: 1820637044
 Session ID: 4256

AFTER SLEEP, I AM NOW TERMINATED:I am the GRANDCHILD
 My ID is: 5079
 Parent ID: 5078
 My Group ID: 1820637044
 Session ID: 4256

===========================================================================================================
./SPOON 2 OUTPUT TEXT (child dies first)

BEFORE SLEEP:I am the CHILD
 My ID is: 4890
 Parent ID: 4889
 My Group ID: 1820637044
 Session ID: 4256

BEFORE SLEEP:I am the GRANDCHILD
 My ID is: 4891
 Parent ID: 4890
 My Group ID: 1820637044
 Session ID: 4256

BEFORE SLEEP:I am the PARENT
 My ID is: 4889
 Parent ID: 4256
 My Group ID: 1820637044
 Session ID: 4256

AFTER SLEEP, I AM NOW TERMINATED:I am the CHILD
 My ID is: 4890
 Parent ID: 4889
 My Group ID: 1820637044
 Session ID: 4256

AFTER SLEEP, I AM NOW TERMINATED:I am the PARENT
 My ID is: 4889
 Parent ID: 4256
 My Group ID: 1820637044
 Session ID: 4256

AFTER SLEEP, I AM NOW TERMINATED:I am the GRANDCHILD
 My ID is: 4891
 Parent ID: 4890
 My Group ID: 1820637044
 Session ID: 4256

===========================================================================================================
./SPOON 1 OUTPUT TEXT (grandchild dies first)

BEFORE SLEEP:I am the CHILD
 My ID is: 5066
 Parent ID: 5065
 My Group ID: 1820637044
 Session ID: 4256

BEFORE SLEEP:I am the GRANDCHILD
 My ID is: 5067
 Parent ID: 5066
 My Group ID: 1820637044
 Session ID: 4256

BEFORE SLEEP:I am the PARENT
 My ID is: 5065
 Parent ID: 4256
 My Group ID: 1820637044
 Session ID: 4256

AFTER SLEEP, I AM NOW TERMINATED:I am the GRANDCHILD
 My ID is: 5067
 Parent ID: 5066
 My Group ID: 1820637044
 Session ID: 4256

AFTER SLEEP, I AM NOW TERMINATED:I am the CHILD
 My ID is: 5066
 Parent ID: 5065
 My Group ID: 1820637044
 Session ID: 4256

AFTER SLEEP, I AM NOW TERMINATED:I am the PARENT
 My ID is: 5065
 Parent ID: 4256
 My Group ID: 1820637044
 Session ID: 4256