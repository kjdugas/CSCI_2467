#include "apue.h"
#include "sys/wait.h"

int main(void) {

    char     buf[MAXLINE];
    pid_t    pid;
    int      status;

    printf("%% ");  // prints out the prompt of our "shell"

    while ( fgets(buf,MAXLINE,stdin) != NULL ) {

        if (buf[strlen(buf)-1] == '\n')
            buf[strlen(buf)-1] = 0;    // replace newline character with null

        if ( (pid = fork() ) < 0 ) 
            err_sys("fork error");
        else if ( pid == 0) {   // this is the forked, child process 
            execlp(buf,buf, (char*)0);
            err_ret("couldn't execute: %s ", buf);
            exit(127);
        }

        // in the parent
        if ( (pid = waitpid(pid, &status, 0)) < 0)
           err_sys("waitpid error");
        printf("%% ");
    }
    exit(0);
}
