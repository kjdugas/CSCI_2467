#include <iostream>

int main() {

   int i = 10;

   std::cout << &i << std::endl;

   int *j = &i;

   std::cout << *j << std::endl;

   (*j)++;

   std::cout << *j << std::endl;
   std::cout << i  << std::endl;
}
