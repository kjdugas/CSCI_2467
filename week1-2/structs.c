#include <stdio.h>

struct date {
     int day;
     int month;
     int year;
};

struct student {
     struct date dob;
     int studentID;
};

int main() {


        struct date myBirthday;
        myBirthday.day = 7;
	myBirthday.month = 10;
        myBirthday.year = 1066;

        struct  student bobJones;
	bobJones.dob = myBirthday;
	bobJones.studentID = 1234356;

	printf("The size of an integer is %u\n",sizeof(int));
	printf("The size of an long is %u\n",sizeof(long));
	printf("The size of an float is %u\n",sizeof(float));
	printf("The size of an double is %u\n",sizeof(double));

}
